# Table Pong

## Test and Deploy
To test this project, simply run the code by either typing:
    1. Inside the terminal --> "python pong.py"
    2. Click the run code button

## Description
This ping pong pygame project is a 2-player game that simulates the classic game of Pong. The game features a ball that bounces back and forth between two paddles, one controlled by a player and an opponent. The objective is to score points by hitting the ball past the opponent's paddle. The player with the most points at the end of the game wins.

The game is played on a black background. The paddles are rectangular and are controlled by the players using the up and down arrow keys on their keyboard.

The game begins with the ball being served from the center of the screen. Once the ball is in play, the players must use their paddles to hit the ball back and forth across the screen. The ball will bounce off the paddles and the walls of the play area until it is missed by one of the players.
